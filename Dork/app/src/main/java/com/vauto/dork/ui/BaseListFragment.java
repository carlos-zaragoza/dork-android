package com.vauto.dork.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.melnykov.fab.FloatingActionButton;
import com.vauto.dork.R;
import com.vauto.dork.adapter.ListAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Base class that shows a list of data and a FAB.
 * An adapter should be initialized in the extending class' constructor.
 * This fragment handles saved state of the list used by the adapter.
 * Created by robert on 3/7/15.
 *
 * @param <L> Item type for the list.
 * @param <A> RecyclerView.Adapter type to use in the list.
 */
public abstract class BaseListFragment<
        L extends Parcelable,
        A extends RecyclerView.Adapter & ListAdapter<L>>
        extends Fragment {

    private static final String TAG = BaseListFragment.TAG;

    static final String ARG_LIST = "arg_list";

    @InjectView(R.id.recycler_view)
    RecyclerView recyclerView;
    @InjectView(R.id.fab)
    FloatingActionButton fab;

    /**
     * Return an adapter to be used by the list.
     *
     * @return An adapter.
     */
    @NonNull
    protected abstract A getAdapter();

    /**
     * Called when the list of data should be loaded from the network.
     */
    protected abstract void loadListFromNetwork();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            ArrayList<L> gamesList = savedInstanceState.getParcelableArrayList(ARG_LIST);
            if (gamesList != null) {
                getAdapter().setList(gamesList);
            } else {
                loadListFromNetwork();
            }
        } else {
            loadListFromNetwork();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_and_fab, container, false);

        ButterKnife.inject(this, v);

        final boolean isInLandscape = getResources().getBoolean(R.bool.is_in_landscape);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(isInLandscape ?
                new GridLayoutManager(getActivity(), 2) :
                new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(getAdapter());

        fab.attachToRecyclerView(recyclerView);
        View.OnClickListener onFabClickListener = getOnFabClickListener();
        if (onFabClickListener != null) {
            fab.setOnClickListener(onFabClickListener);
        }

        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(ARG_LIST, getAdapter().getList());
        super.onSaveInstanceState(outState);
    }

    /**
     * Get the click listener that will be used when the FAB is clicked;
     *
     * @return Return a click listener to use. If null then no click listener is used.
     */
    @Nullable
    public View.OnClickListener getOnFabClickListener() {
        return null;
    }
}
