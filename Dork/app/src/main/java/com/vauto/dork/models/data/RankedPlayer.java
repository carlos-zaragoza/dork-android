package com.vauto.dork.models.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Model for a player with a rank.
 * Created by robert on 3/8/15.
 */
public class RankedPlayer extends BaseModel<RankedPlayer> implements Parcelable {
    /**
     * Used when the rank is unknown or hasn't been marked.
     */
    public static final int PLACE_UNKNOWN = 0;
    /**
     * Format used for rating.
     */
    public static NumberFormat RATING_FORMAT = new DecimalFormat("#.##");
    private Player player;
    private int totalPoints;
    private int gamesPlayed;
    private int rank;

    public RankedPlayer() {
        rank = PLACE_UNKNOWN;
    }

    public RankedPlayer(Player player, int totalPoints) {
        this();
        this.player = player;
        this.totalPoints = totalPoints;
    }

    public RankedPlayer(Parcel source) {
        super(source);
        player = source.readParcelable(Player.class.getClassLoader());
        totalPoints = source.readInt();
        gamesPlayed = source.readInt();
        rank = source.readInt();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * Get the player's rating.
     *
     * @return Rating.
     */
    public float getRating() {
        if (gamesPlayed > 0f) {
            return (float) totalPoints / (float) gamesPlayed;
        }
        return 0f;
    }

    /**
     * Get the string formatted rating for the player.
     *
     * @return Formatted rating.
     */
    public String getRatingFormatted() {
        return RATING_FORMAT.format(getRating());
    }

    @Override
    public void fromModel(RankedPlayer model) {
        super.fromModel(model);
        player = model.player;
        totalPoints = model.totalPoints;
        gamesPlayed = model.gamesPlayed;
        rank = model.rank;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(player, flags);
        dest.writeInt(totalPoints);
        dest.writeInt(gamesPlayed);
        dest.writeInt(rank);
    }

    public static final Creator<RankedPlayer> CREATOR = new Creator<RankedPlayer>() {
        @Override
        public RankedPlayer createFromParcel(Parcel source) {
            return new RankedPlayer(source);
        }

        @Override
        public RankedPlayer[] newArray(int size) {
            return new RankedPlayer[0];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RankedPlayer that = (RankedPlayer) o;

        if (totalPoints != that.totalPoints) return false;
        if (gamesPlayed != that.gamesPlayed) return false;
        if (rank != that.rank) return false;
        return !(player != null ? !player.equals(that.player) : that.player != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (player != null ? player.hashCode() : 0);
        result = 31 * result + totalPoints;
        result = 31 * result + gamesPlayed;
        result = 31 * result + rank;
        return result;
    }

    @Override
    public String toString() {
        return "RankedPlayer{" +
                "player=" + player +
                ", totalPoints=" + totalPoints +
                ", gamesPlayed=" + gamesPlayed +
                ", rank=" + rank +
                "} " + super.toString();
    }
}
