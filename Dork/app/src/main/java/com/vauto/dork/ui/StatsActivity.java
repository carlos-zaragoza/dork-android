package com.vauto.dork.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.vauto.dork.R;

public class StatsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment_container);

        if (savedInstanceState == null) {
            StatsFragment fragment = new StatsFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment, StatsFragment.TAG)
                    .commit();
        }
    }
}
