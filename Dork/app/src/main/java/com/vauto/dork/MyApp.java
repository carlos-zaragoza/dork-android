package com.vauto.dork;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vauto.dork.network.GamesService;
import com.vauto.dork.network.PlayersService;
import com.vauto.dork.utils.DateTypeAdapter;

import java.util.Date;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Custom application class.
 * Created by robert on 3/4/15.
 */
public class MyApp extends Application {
    private static MyApp instance = null;
    private static RestAdapter sRestAdapter = null;
    private static GamesService sGamesService = null;
    private static PlayersService sPlayersService = null;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateTypeAdapter())
                .create();

        sRestAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(getString(R.string.host_url))
                .setConverter(new GsonConverter(gson))
                .build();
    }

    public static GamesService getGamesService() {
        if (sGamesService == null) {
            sGamesService = sRestAdapter.create(GamesService.class);
        }
        return sGamesService;
    }

    public static PlayersService getPlayersService() {
        if (sPlayersService == null) {
            sPlayersService = sRestAdapter.create(PlayersService.class);
        }
        return sPlayersService;
    }
}
