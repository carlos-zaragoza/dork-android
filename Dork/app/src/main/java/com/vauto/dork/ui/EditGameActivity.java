package com.vauto.dork.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.vauto.dork.R;
import com.vauto.dork.models.data.Game;

public class EditGameActivity extends ActionBarActivity implements EditGameFragment.Callbacks {

    public static final String ARG_GAME = "arg_game";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment_container);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new EditGameFragment())
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        if (android.R.id.home == item.getItemId()) {
            onBackPressed();
            handled = true;
        }
        return handled || super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveSuccess(@NonNull Game game) {
        Intent intent = new Intent();
        intent.putExtra(ARG_GAME, game);
        setResult(RESULT_OK, intent);
        finish();
    }
}
