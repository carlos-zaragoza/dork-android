package com.vauto.dork.models;

/**
 * Created by robert on 3/4/15.
 */
public enum DorkType {
    /**
     * Player that earned the highest points per game ratio in a month.
     */
    UberDork,
    /**
     * Player that has a negative points per game ratio for a month.
     */
    MegaDork,
}
