package com.vauto.dork.network;

import com.vauto.dork.models.PlayerSortType;
import com.vauto.dork.models.data.Player;
import com.vauto.dork.models.data.RankedPlayer;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Service to interact with players.
 * Created by robert on 4/7/15.
 */
public interface PlayersService {
    @GET("/players/")
    ArrayList<Player> getPlayers(@Query("sort") boolean sortByDisplayName);

    @GET("/players/")
    void getPlayers(@Query("sort") boolean sortByDisplayName, Callback<ArrayList<Player>> cb);

    /**
     * Sort a list of players. Most useful to sort by anything other than {@link PlayerSortType#Alphabetical}.
     *
     * @param month Month integer, zero based (0 = Jan, 1 = Feb, etc...)
     * @param year  Year integer.
     * @param playerSortType Sort type to sort by. TODO use enum and have gson convert.
     * @param players        Players to sort.
     * @param cb             Callback.
     */
    @POST("/players/sort/")
    void sortPlayers(@Query("month") int month, @Query("year") int year, @Query("sortType") int playerSortType, @Body ArrayList<Player> players, Callback<ArrayList<Player>> cb);

    @GET("/players/{id}")
    Player getPlayer(@Path("id") String playerId);

    @POST("/players/")
    Player savePlayer(@Body Player player);

    @POST("/players/")
    void savePlayer(@Body Player player, Callback<Player> cb);

    @PUT("/players/{id}")
    Response updatePlayer(@Path("id") String playerId, @Body Player player);

    @DELETE("/players/{id}")
    Response deletePlayer(@Path("id") String playerId);

    /**
     * Get ranked players in a time span
     *
     * @param month Month integer, zero based (0 = Jan, 1 = Feb, etc...)
     * @param year  Year integer.
     * @return A list of ranked players for the time span.
     */
    @GET("/players/ranked/")
    ArrayList<RankedPlayer> getRankedPlayers(@Query("month") int month, @Query("year") int year);

    /**
     * Get ranked players in a time span
     *
     * @param month Month integer, zero based (0 = Jan, 1 = Feb, etc...)
     * @param year  Year integer.
     * @param cb    Callback to receive the list of ranked players.
     */
    @GET("/players/ranked/")
    void getRankedPlayers(@Query("month") int month, @Query("year") int year, Callback<ArrayList<RankedPlayer>> cb);

    /**
     * Get ranked players in a time span
     *
     * @param year Year integer.
     * @param cb   Callback to receive the list of ranked players.
     */
    @GET("/players/ranked/")
    void getRankedPlayers(@Query("year") int year, Callback<ArrayList<RankedPlayer>> cb);
}
