package com.vauto.dork.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vauto.dork.R;
import com.vauto.dork.models.data.RankedPlayer;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Adapter used to display a list of ranked players.
 * Created by robert on 4/14/15.
 */
public class RankedPlayerListAdapter
        extends RecyclerView.Adapter<RankedPlayerListAdapter.ViewHolder>
        implements ListAdapter<RankedPlayer> {

    private ArrayList<RankedPlayer> list;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ranked_player_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RankedPlayer rankedPlayer = list.get(position);
        holder.nameAbbr.setText(rankedPlayer.getPlayer().getAbbreviatedName());
        holder.name.setText(rankedPlayer.getPlayer().getDisplayName());
        holder.rank.setText(String.valueOf(rankedPlayer.getRank()));
        holder.rating.setText(rankedPlayer.getRatingFormatted());
        holder.pointsPerGame.setText(
                String.format("%s / %s", rankedPlayer.getTotalPoints(), rankedPlayer.getGamesPlayed()));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Nullable
    @Override
    public ArrayList<RankedPlayer> getList() {
        return list;
    }

    @Override
    public void setList(@Nullable ArrayList<RankedPlayer> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.rank)
        TextView rank;
        @InjectView(R.id.name_abbr)
        TextView nameAbbr;
        @InjectView(R.id.name)
        TextView name;
        @InjectView(R.id.rating)
        TextView rating;
        @InjectView(R.id.points_per_game)
        TextView pointsPerGame;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
