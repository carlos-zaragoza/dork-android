package com.vauto.dork.network;

import com.vauto.dork.models.data.Game;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Service to interact with games.
 * Created by robert on 4/7/15.
 */
public interface GamesService {
    @GET("/games/")
    ArrayList<Game> getGames();

    @GET("/games/")
    void getGames(Callback<ArrayList<Game>> cb);

    @GET("/games/{id}")
    Game getGame(@Path("id") String gameId);

    @POST("/games/")
    Game saveGame(@Body Game game);

    @POST("/games/")
    void saveGame(@Body Game game, Callback<Game> cb);

    @PUT("/games/{id}")
    Response updateGame(@Path("id") String gameId, @Body Game game);

    @DELETE("/games/{id}")
    Response deleteGame(@Path("id") String gameId);
}
