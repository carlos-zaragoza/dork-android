package com.vauto.dork.models.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.vauto.dork.utils.ParcelUtils;

import java.util.ArrayList;
import java.util.Date;

public class Game extends BaseModel<Game> implements Parcelable {

    private Player winner;
    private Date datePlayed;
    private ArrayList<GamePlayer> players;

    public Game() {
    }

    public Game(Parcel source) {
        super(source);
        this.winner = source.readParcelable(Player.class.getClassLoader());
        this.datePlayed = ParcelUtils.readDate(source);
        this.players = source.readArrayList(Player.class.getClassLoader());
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public Date getDatePlayed() {
        return datePlayed;
    }

    public void setDatePlayed(Date datePlayed) {
        this.datePlayed = datePlayed;
    }

    public ArrayList<GamePlayer> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<GamePlayer> players) {
        this.players = players;
    }

    @Override
    public void fromModel(Game model) {
        super.fromModel(model);
        this.winner = model.winner;
        this.datePlayed = model.datePlayed;
        this.players = model.players;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(getWinner(), flags);
        ParcelUtils.writeDate(dest, getDatePlayed());
        dest.writeList(players);
    }

    public static final Creator<Game> CREATOR = new Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel source) {
            return new Game(source);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[0];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Game game = (Game) o;

        if (winner != null ? !winner.equals(game.winner) : game.winner != null) return false;
        if (datePlayed != null ? !datePlayed.equals(game.datePlayed) : game.datePlayed != null)
            return false;
        return !(players != null ? !players.equals(game.players) : game.players != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (winner != null ? winner.hashCode() : 0);
        result = 31 * result + (datePlayed != null ? datePlayed.hashCode() : 0);
        result = 31 * result + (players != null ? players.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Game{" +
                "winner=" + winner +
                ", datePlayed=" + datePlayed +
                ", players=" + players +
                "} " + super.toString();
    }
}
