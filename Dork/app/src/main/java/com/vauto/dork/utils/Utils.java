package com.vauto.dork.utils;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * General utility methods.
 * Created by robert on 3/5/15.
 */
public class Utils {

    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd, yy");

    public static String getFormattedDate(Date date) {
        if (date != null) {
            return simpleDateFormat.format(date);
        }
        return "";
    }

    @NonNull
    public static Date getParsedDate(@NonNull String string) {
        try {
            return simpleDateFormat.parse(string);
        } catch (ParseException e) {
            throw new RuntimeException("Couldn't parse date: \"" + string + "\" : ", e);
        }
    }
}
