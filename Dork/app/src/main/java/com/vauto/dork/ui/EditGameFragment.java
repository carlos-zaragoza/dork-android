package com.vauto.dork.ui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.vauto.dork.MyApp;
import com.vauto.dork.R;
import com.vauto.dork.adapter.GamePlayersListAdapter;
import com.vauto.dork.models.data.Game;
import com.vauto.dork.models.data.GamePlayer;
import com.vauto.dork.models.data.Player;
import com.vauto.dork.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Fragment to edit an existing game or create a new game.
 * Created by robert on 3/7/15.
 */
public class EditGameFragment extends Fragment implements GamePlayersListAdapter.Callbacks {
    public static final String TAG = EditGameFragment.class.getSimpleName();

    private static final String ARG_GAME = "arg_game";
    private static final String ARG_SELECTED_PLAYERS = "arg_selected_players";
    private static final String ARG_IS_SAVING = "arg_is_saving";
    private static final int REQ_PLAYER_SELECT = 1;

    @InjectView(R.id.date_picker_button)
    Button datePickerButton;
    @InjectView(R.id.select_players)
    Button selectPlayersButton;
    @InjectView(R.id.recycler_view)
    RecyclerView recyclerView;

    private Callbacks callbacks;
    private Game game;
    private boolean isSaving;

    private GamePlayersListAdapter adapter;

    public EditGameFragment() {
        isSaving = false;
        adapter = new GamePlayersListAdapter(this);
        game = new Game();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callbacks = (Callbacks) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_game, container, false);
        ButterKnife.inject(this, rootView);

        datePickerButton.setOnClickListener(onClickListener);
        selectPlayersButton.setOnClickListener(onClickListener);

        Calendar c = Calendar.getInstance(Locale.getDefault());
        if (savedInstanceState != null) {
            game = savedInstanceState.getParcelable(ARG_GAME);
            c.setTime(game.getDatePlayed());
            adapter.setList(savedInstanceState.<GamePlayer>getParcelableArrayList(ARG_SELECTED_PLAYERS));
            isSaving = savedInstanceState.getBoolean(ARG_IS_SAVING);
        }
        onDateSetListener.onDateSet(null, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        game.setDatePlayed(c.getTime());

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.game, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        boolean canSave = adapter != null
                && adapter.getList() != null
                && adapter.hasThreeInTheMoney();

        menu.findItem(R.id.menu_save).setEnabled(!isSaving && canSave);
        datePickerButton.setEnabled(!isSaving);
        selectPlayersButton.setEnabled(!isSaving);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        if (item.getItemId() == R.id.menu_save) {
            save();
            handled = true;
        }
        return handled || super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(ARG_GAME, game);
        outState.putParcelableArrayList(ARG_SELECTED_PLAYERS, adapter.getList());
        outState.putBoolean(ARG_IS_SAVING, isSaving);
        super.onSaveInstanceState(outState);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == datePickerButton.getId()) {
                showDatePicker();
            } else if (v.getId() == selectPlayersButton.getId()) {
                Intent intent = new Intent(getActivity(), PlayersActivity.class);
                intent.putExtra(PlayersActivity.ARG_SELECTED_PLAYERS, findSelectedPlayers());
                startActivityForResult(intent, REQ_PLAYER_SELECT);
            }
        }
    };

    private void showDatePicker() {
        DatePickerDialogFragment datePickerDialogFragment = new DatePickerDialogFragment();
        datePickerDialogFragment.show(getFragmentManager(), DatePickerDialogFragment.TAG);
        datePickerDialogFragment.setOnDateSetListener(onDateSetListener);
    }

    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar c = Calendar.getInstance(Locale.getDefault());
            c.set(year, monthOfYear, dayOfMonth);
            Date date = c.getTime();
            game.setDatePlayed(date);
            final String gameDate = Utils.getFormattedDate(date);
            datePickerButton.setText(gameDate);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_PLAYER_SELECT && resultCode == Activity.RESULT_OK) {
            ArrayList<Player> selectedPlayersList = data.getParcelableArrayListExtra(PlayersActivity.ARG_SELECTED_PLAYERS);
            ArrayList<GamePlayer> gamePlayerList = adapter.getList();

            if (gamePlayerList == null) {
                // no game players yet so add all of the selected players
                gamePlayerList = new ArrayList<>(selectedPlayersList.size());
            } else {
                int size = gamePlayerList.size();
                for (int i = 0; i < size; i++) {
                    // if the game players list has a player no longer selected then remove it
                    Player player = gamePlayerList.get(i).getPlayer();
                    if (!selectedPlayersList.contains(player)) {
                        gamePlayerList.remove(i--);
                        size--;
                    } else {
                        selectedPlayersList.remove(player);
                    }
                }
            }
            for (Player selectedPlayer : selectedPlayersList) {
                // if the selected player list has a player not in the game players list then add it
                if (!containsPlayer(gamePlayerList, selectedPlayer)) {
                    GamePlayer gamePlayer = new GamePlayer(selectedPlayer, 0);
                    gamePlayerList.add(gamePlayer);
                }
            }
            adapter.setList(gamePlayerList);
            getActivity().supportInvalidateOptionsMenu();
        }
    }

    private boolean containsPlayer(@NonNull ArrayList<GamePlayer> gamePlayerList, @NonNull Player player) {
        for (GamePlayer gamePlayer : gamePlayerList) {
            if (gamePlayer.getPlayer().equals(player)) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    private ArrayList<Player> findSelectedPlayers() {
        ArrayList<GamePlayer> gamePlayerList = adapter.getList();
        if (gamePlayerList == null) {
            return null;
        }
        ArrayList<Player> selectedPlayers = new ArrayList<>(gamePlayerList.size());

        for (GamePlayer gamePlayer : gamePlayerList) {
            selectedPlayers.add(gamePlayer.getPlayer());
        }
        return selectedPlayers;
    }

    @Override
    public void moneyChange() {
        if (getActivity() != null) {
            getActivity().supportInvalidateOptionsMenu();
        }
    }

    public void save() {
        isSaving = true;
        getActivity().supportInvalidateOptionsMenu();
        game.setWinner(adapter.getPlayerInFirstPlace());
        ArrayList<GamePlayer> gamePlayerList = adapter.getList();
        assert (gamePlayerList != null);
        game.setPlayers(gamePlayerList);

        MyApp.getGamesService().saveGame(game, new Callback<Game>() {
            @Override
            public void success(Game game, Response response) {
                callbacks.onSaveSuccess(game);
                done(R.string.game_saved);
            }

            @Override
            public void failure(RetrofitError error) {
                done(R.string.error_saving);
                Log.e(TAG, "error: " + error.getKind().toString() + " : " + error.toString());
            }

            private void done(@StringRes int errMsgResId) {
                Toast.makeText(getActivity(), errMsgResId, Toast.LENGTH_SHORT).show();
                isSaving = false;
                getActivity().supportInvalidateOptionsMenu();
            }
        });
    }

    public interface Callbacks {
        /**
         * Called when the game has successfully been saved.
         */
        public void onSaveSuccess(@NonNull Game game);
    }
}
