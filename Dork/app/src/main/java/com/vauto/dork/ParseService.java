package com.vauto.dork;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ParseService extends IntentService {
    public static final String TAG = ParseService.class.getSimpleName();
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.vauto.dork.action.FOO";
    private static final String ACTION_GET_ALL_GAMES = "com.vauto.dork.action.get_all_games";
    private static final String ACTION_GET_ALL_PLAYERS = "com.vauto.dork.action.get_all_players";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.vauto.dork.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.vauto.dork.extra.PARAM2";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, ParseService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    public static void startActionGetAllGames(Context context) {
        Intent intent = new Intent(context, ParseService.class);
        intent.setAction(ACTION_GET_ALL_GAMES);
        context.startService(intent);
    }

    public static void startActionGetAllPlayers(Context context) {
        Intent intent = new Intent(context, ParseService.class);
        intent.setAction(ACTION_GET_ALL_PLAYERS);
        context.startService(intent);
    }

    public ParseService() {
        super("ParseService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionFoo(param1, param2);
            } else if (ACTION_GET_ALL_GAMES.equals(action)) {
                handleActionGetAllGames();
            } else if (ACTION_GET_ALL_PLAYERS.equals(action)) {
                handleActionGetAllPlayers();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void handleActionGetAllGames() {

    }

    private void handleActionGetAllPlayers() {
        ParseServiceManager.sUpdateServiceManager.threadPool.execute(new Runnable() {
            @Override
            public void run() {

            }
        });
    }
}
