package com.vauto.dork.models.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class Player extends BaseModel<Player> implements Parcelable {
    private String firstName;
    private String lastName;
    private String nickname;

    public Player() {
    }

    public Player(String firstName, String lastName, String nickname) {
        this();
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickname = nickname;
    }

    public Player(Parcel source) {
        super(source);
        firstName = source.readString();
        lastName = source.readString();
        nickname = source.readString();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * Returns the concatenation of the player's first and last name.
     *
     * @return A player's first and last name.
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }

    /**
     * Returns a name for the player. The player's nickname will be used if there is one.
     * Otherwise the player's full name is returned.
     *
     * @return Returns a player's display name.
     */
    public String getDisplayName() {
        return TextUtils.isEmpty(getNickname()) ? getFullName() : getNickname();
    }

    /**
     * Returns a display name with the abbreviated name.
     *
     * @return A display name with an abbreviation.
     * @see #getAbbreviatedName()
     * @see #getDisplayName()
     */
    public String getAbbrDisplayName() {
        return String.format("%s %s", getAbbreviatedName(), getDisplayName());
    }

    /**
     * Returns an abbreviated name. Ex: Robert Szabo would return (RS)
     *
     * @return An abbreviated name.
     */
    public String getAbbreviatedName() {
        return "(" + getFirstName().charAt(0) + getLastName().charAt(0) + ")";
    }

    @Override
    public void fromModel(Player model) {
        super.fromModel(model);
        firstName = model.firstName;
        lastName = model.lastName;
        nickname = model.nickname;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(nickname);
    }

    public static final Creator<Player> CREATOR = new Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel source) {
            return new Player(source);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[0];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Player player = (Player) o;

        if (firstName != null ? !firstName.equals(player.firstName) : player.firstName != null)
            return false;
        if (lastName != null ? !lastName.equals(player.lastName) : player.lastName != null)
            return false;
        if (nickname != null ? !nickname.equals(player.nickname) : player.nickname != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Player{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", nickname='" + nickname + '\'' +
                "} " + super.toString();
    }
}
