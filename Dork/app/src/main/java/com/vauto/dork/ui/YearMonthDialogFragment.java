package com.vauto.dork.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.vauto.dork.R;

import java.util.Calendar;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Dialog fragment to pick a month and year.
 * Created by robert on 4/19/15.
 */
public class YearMonthDialogFragment extends DialogFragment {
    public static final String TAG = YearMonthDialogFragment.class.getSimpleName();
    public static final int ALL_MONTHS = -1;
    @InjectView(R.id.month)
    Spinner monthSpinner;
    @InjectView(R.id.year)
    Spinner yearSpinner;

    private static final Integer[] YEARS;

    static {
        Calendar now = Calendar.getInstance(Locale.getDefault());
        final int firstYear = 2015;
        final int lastYear = now.get(Calendar.YEAR);
        final int years = lastYear - firstYear + 1;
        YEARS = new Integer[years];
        for (int i = 0; i < years; i++) {
            YEARS[i] = firstYear + i;
        }
    }

    private Callbacks callbacks;

    private static final String ARG_MONTH = "arg_month";
    private static final String ARG_YEAR = "arg_year";


    public static YearMonthDialogFragment newInstance(int month, int year) {
        YearMonthDialogFragment fragment = new YearMonthDialogFragment();
        Bundle args = new Bundle(2);
        args.putInt(ARG_MONTH, month);
        args.putInt(ARG_YEAR, year);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.date_range)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callbacks != null) {
                            int month = monthSpinner.getSelectedItemPosition();
                            if (month >= 12) {
                                month = ALL_MONTHS;
                            }
                            int year = YEARS[yearSpinner.getSelectedItemPosition()];
                            callbacks.onDateRangeSelected(month, year);
                        }
                    }
                })
                .create();

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.year_month_picker, null);
        dialog.setView(v);

        ButterKnife.inject(this, v);

        int month = getArguments().getInt(ARG_MONTH);
        monthSpinner.setSelection(month == ALL_MONTHS ? 12 : month);

        ArrayAdapter<Integer> yearAdapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, YEARS);
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(yearAdapter);
        int year = getArguments().getInt(ARG_YEAR);
        int yearIndex = YEARS.length - 1;
        for (int i = 0; i < YEARS.length; i++) {
            if (year == YEARS[i]) {
                yearIndex = i;
                break;
            }
        }
        yearSpinner.setSelection(yearIndex);

        return dialog;
    }

    public void setCallbacks(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    public interface Callbacks {
        /**
         * Called when the date has been selected
         *
         * @param month Zero based (0 = January, 1 = February, etc...)
         * @param year  Year selected.
         */
        void onDateRangeSelected(int month, int year);
    }
}
