package com.vauto.dork.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;
import com.vauto.dork.MyApp;
import com.vauto.dork.R;
import com.vauto.dork.adapter.PlayersListAdapter;
import com.vauto.dork.adapter.RankedPlayerListAdapter;
import com.vauto.dork.models.data.Player;
import com.vauto.dork.models.data.RankedPlayer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class StatsFragment extends Fragment
        implements EditPlayerDialogFragment.Callbacks,
        YearMonthDialogFragment.Callbacks {

    public static final String TAG = StatsFragment.class.getSimpleName();
    private boolean isRankedVisible;
    private RankedPlayerListAdapter rankedPlayersAdapter;
    private PlayersListAdapter unRankedPlayersAdapter;

    @InjectView(R.id.recycler_view)
    RecyclerView recyclerView;
    @InjectView(R.id.fab)
    FloatingActionButton fab;

    private int month;
    private int year;

    private static final String ARG_IS_RANKED_VISIBLE = "arg_is_ranked_visible";
    private static final String ARG_LIST_RANKED = "arg_list_ranked";
    private static final String ARG_LIST_UN_RANKED = "arg_list_un_ranked";
    private static final String ARG_MONTH = "arg_month";
    private static final String ARG_YEAR = "arg_year";

    public StatsFragment() {
        rankedPlayersAdapter = new RankedPlayerListAdapter();
        unRankedPlayersAdapter = new PlayersListAdapter(null, false);
        isRankedVisible = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState != null) {
            month = savedInstanceState.getInt(ARG_MONTH);
            year = savedInstanceState.getInt(ARG_YEAR);
            isRankedVisible = savedInstanceState.getBoolean(ARG_IS_RANKED_VISIBLE, isRankedVisible);
            ArrayList<RankedPlayer> rankedPlayers = savedInstanceState.getParcelableArrayList(ARG_LIST_RANKED);
            if (rankedPlayers != null) {
                rankedPlayersAdapter.setList(rankedPlayers);
                ArrayList<Player> players = savedInstanceState.getParcelableArrayList(ARG_LIST_UN_RANKED);
                unRankedPlayersAdapter.setList(players);
            } else {
                loadListFromNetwork(month, year);
            }
        } else {
            Calendar c = Calendar.getInstance(Locale.getDefault());
            loadListFromNetwork(c.get(Calendar.MONTH), c.get(Calendar.YEAR));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_and_fab, container, false);

        ButterKnife.inject(this, v);

        final boolean isInLandscape = getResources().getBoolean(R.bool.is_in_landscape);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(isInLandscape ?
                new GridLayoutManager(getActivity(), 2) :
                new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(getAdapter());

        fab.setOnClickListener(onFabClickListener);

        return v;
    }

    protected void loadListFromNetwork(int month, int year) {
        this.month = month;
        this.year = year;
        Callback<ArrayList<RankedPlayer>> cb = new Callback<ArrayList<RankedPlayer>>() {
            @Override
            public void success(ArrayList<RankedPlayer> players, Response response) {
                ArrayList<RankedPlayer> rankedList = new ArrayList<>();
                ArrayList<Player> unRankedList = new ArrayList<>();
                for (RankedPlayer player : players) {
                    if (player.getRank() > 0) {
                        rankedList.add(player);
                    } else {
                        unRankedList.add(player.getPlayer());
                    }
                }
                rankedPlayersAdapter.setList(rankedList);
                unRankedPlayersAdapter.setList(unRankedList);
            }

            @Override
            public void failure(RetrofitError error) {
                String message = getString(R.string.error_getting_ranked_players) + " : " + error.getKind();
                Toast.makeText(getActivity(), R.string.error_getting_ranked_players, Toast.LENGTH_SHORT).show();
                Log.e(TAG, message);
            }
        };
        if (month == YearMonthDialogFragment.ALL_MONTHS) {
            MyApp.getPlayersService().getRankedPlayers(year, cb);
        } else {
            MyApp.getPlayersService().getRankedPlayers(month, year, cb);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.ratings, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_ranked).setVisible(!isRankedVisible);
        menu.findItem(R.id.menu_un_ranked).setVisible(isRankedVisible);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;

        switch (item.getItemId()) {
            case R.id.menu_ranked:
                isRankedVisible = true;
                adapterChange();
                break;
            case R.id.menu_un_ranked:
                isRankedVisible = false;
                adapterChange();
                break;
            case R.id.menu_filter:
                YearMonthDialogFragment yearMonthDialogFragment = YearMonthDialogFragment.newInstance(month, year);
                yearMonthDialogFragment.setCallbacks(this);
                yearMonthDialogFragment.show(getFragmentManager(), YearMonthDialogFragment.TAG);
                break;
            default:
                handled = false;
        }

        return handled || super.onOptionsItemSelected(item);
    }

    private void adapterChange() {
        recyclerView.setAdapter(getAdapter());
        getActivity().supportInvalidateOptionsMenu();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(ARG_MONTH, month);
        outState.putInt(ARG_YEAR, year);
        outState.putBoolean(ARG_IS_RANKED_VISIBLE, isRankedVisible);
        outState.putParcelableArrayList(ARG_LIST_RANKED, rankedPlayersAdapter.getList());
        super.onSaveInstanceState(outState);
    }

    protected RecyclerView.Adapter<?> getAdapter() {
        return isRankedVisible ? rankedPlayersAdapter : unRankedPlayersAdapter;
    }

    private View.OnClickListener onFabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EditPlayerDialogFragment dialogFragment = new EditPlayerDialogFragment();
            dialogFragment.setCallbacks(StatsFragment.this);
            dialogFragment.show(getFragmentManager(), EditPlayerDialogFragment.TAG);
        }
    };

    @Override
    public void onPlayerSaved(@NonNull Player player) {
        ArrayList<Player> players = unRankedPlayersAdapter.getList();
        if (players == null) {
            players = new ArrayList<>(1);
            unRankedPlayersAdapter.setList(players);
        }
        players.add(player);
        unRankedPlayersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDateRangeSelected(int month, int year) {
        loadListFromNetwork(month, year);
    }
}
