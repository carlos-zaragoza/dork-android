package com.vauto.dork.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.vauto.dork.R;
import com.vauto.dork.models.data.Player;

import java.util.ArrayList;

public class PlayersActivity extends ActionBarActivity implements PlayerSelectFragment.Callbacks {

    public static final String ARG_SELECTED_PLAYERS = "arg_selected_players";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment_container);

        if (savedInstanceState == null) {
            ArrayList<Player> selectedPlayers = getIntent().getParcelableArrayListExtra(ARG_SELECTED_PLAYERS);
            PlayerSelectFragment playerSelectFragment = PlayerSelectFragment.newInstance(selectedPlayers);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, playerSelectFragment, PlayerSelectFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        if (android.R.id.home == item.getItemId()) {
            onBackPressed();
            handled = true;
        }
        return handled || super.onOptionsItemSelected(item);
    }

    @Override
    public void onPlayersSelected(ArrayList<Player> selectedPlayersList) {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(ARG_SELECTED_PLAYERS, selectedPlayersList);
        setResult(RESULT_OK, intent);
        finish();
    }
}
