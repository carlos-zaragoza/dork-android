package com.vauto.dork.models.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public abstract class BaseModel<U extends BaseModel> implements Parcelable {
    @SerializedName("_id")
    private String objectId;

    public BaseModel() {
    }

    public BaseModel(Parcel source) {
        this();
        objectId = source.readString();
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public void fromModel(U model) {
        this.objectId = model.getObjectId();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(objectId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseModel<?> baseModel = (BaseModel<?>) o;

        return !(objectId != null ? !objectId.equals(baseModel.objectId) : baseModel.objectId != null);

    }

    @Override
    public int hashCode() {
        return objectId != null ? objectId.hashCode() : 0;
    }


}
