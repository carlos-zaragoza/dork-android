package com.vauto.dork.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vauto.dork.R;
import com.vauto.dork.models.data.Player;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Adapter to be used on a list with player data
 * Created by robert on 3/8/15.
 */
public class PlayersListAdapter
        extends RecyclerView.Adapter<PlayersListAdapter.ViewHolder>
        implements ListAdapter<Player> {

    private Callbacks callbacks;
    private ArrayList<Player> playerList;
    private ArrayList<Integer> selectedItems;
    private ArrayList<Player> preLoadedSelectedList;
    private static final String ARG_SELECTED_ITEMS = "arg_selected_items";
    private static final String ARG_PRELOADED_SELECTED_LIST = "arg_preloaded_selected_list";
    private boolean itemsSelectable;

    public PlayersListAdapter(@Nullable Callbacks callbacks) {
        this(callbacks, true);
    }

    /**
     * @param callbacks
     * @param itemsSelectable True if the items are selectable, false if not.
     */
    public PlayersListAdapter(@Nullable Callbacks callbacks, boolean itemsSelectable) {
        this.callbacks = callbacks;
        this.itemsSelectable = itemsSelectable;
    }

    /**
     * Set a list of players that should be selected by default.
     * This should be called before {@link #setList(java.util.ArrayList)}.
     *
     * @param preLoadedSelectedList List of players to select by default.
     */
    public void preLoadedSelectedList(@Nullable ArrayList<Player> preLoadedSelectedList) {
        if (preLoadedSelectedList != null && preLoadedSelectedList.size() > 0) {
            this.preLoadedSelectedList = preLoadedSelectedList;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.text1)
        TextView name1;
        @InjectView(R.id.text2)
        TextView name2;
        @InjectView(R.id.stats)
        ViewGroup stats;
        Player player;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            if (itemsSelectable) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final boolean isActivated = !v.isActivated();
                        setActivated(isActivated);
                    }
                });
            }
        }

        public void setActivated(boolean isActivated) {
            if (itemView.isActivated() != isActivated) {
                itemView.setActivated(isActivated);
                boolean listUpdated = false;
                if (isActivated) {
                    if (!selectedItems.contains(getPosition())) {
                        selectedItems.add(getPosition());
                        listUpdated = true;
                    }
                } else {
                    selectedItems.remove(Integer.valueOf(getPosition()));
                    listUpdated = true;
                }
                if (listUpdated && callbacks != null) {
                    callbacks.onPlayerSelectionChange(selectedItems.size());
                }
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (itemsSelectable) {
            holder.setActivated(selectedItems.contains(position));
        }
        Player player = getItem(position);
        holder.player = player;
        final String displayName = player.getAbbrDisplayName();
        holder.name1.setText(displayName);
        if (TextUtils.isEmpty(player.getNickname())) {
            // no nickname
            holder.name2.setVisibility(View.GONE);
        } else {
            // nickname exists
            holder.name2.setText(player.getFullName());
            holder.name2.setVisibility(View.VISIBLE);
        }

        holder.stats.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return playerList == null ? 0 : playerList.size();
    }

    @NonNull
    Player getItem(final int position) {
        return playerList.get(position);
    }

    @Nullable
    @Override
    public ArrayList<Player> getList() {
        return playerList;
    }

    /**
     * Get a list consisting of the selected players.
     *
     * @return List of selected players.
     */
    @NonNull
    public ArrayList<Player> getSelectedPlayersList() {
        if (selectedItems != null) {
            ArrayList<Player> selectedPlayers = new ArrayList<>(selectedItems.size());
            for (Integer selectedItemIndex : selectedItems) {
                selectedPlayers.add(playerList.get(selectedItemIndex));
            }
            return selectedPlayers;
        }
        return new ArrayList<>(0);
    }

    @Override
    public void setList(@Nullable ArrayList<Player> list) {
        this.playerList = list;
        if (list != null) {
            final int count = list.size();
            selectedItems = new ArrayList<>(count);
            if (preLoadedSelectedList != null) {
                int size = list.size();
                // all players in the preloaded list should be in the list being set.
                for (int i = 0; i < size; i++) {
                    Player player = list.get(i);
                    if (preLoadedSelectedList.contains(player)) {
                        if (itemsSelectable) {
                            selectedItems.add(i);
                        }
                        preLoadedSelectedList.remove(player);
                    }
                }
                preLoadedSelectedList = null;
            }
        } else {
            if (selectedItems != null) {
                selectedItems.clear();
            } else {
                selectedItems = new ArrayList<>();
            }
        }
        if (callbacks != null && itemsSelectable) {
            callbacks.onPlayerSelectionChange(selectedItems.size());
        }
        notifyDataSetChanged();
    }

    public void onSaveInstanceState(@NonNull Bundle outstate) {
        outstate.putIntegerArrayList(ARG_SELECTED_ITEMS, selectedItems);
        outstate.putParcelableArrayList(ARG_PRELOADED_SELECTED_LIST, preLoadedSelectedList);
    }

    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            selectedItems = savedInstanceState.getIntegerArrayList(ARG_SELECTED_ITEMS);
            preLoadedSelectedList = savedInstanceState.getParcelableArrayList(ARG_PRELOADED_SELECTED_LIST);
        }
        if (callbacks != null && itemsSelectable) {
            callbacks.onPlayerSelectionChange(selectedItems.size());
        }
        notifyDataSetChanged();
    }

    public interface Callbacks {
        /**
         * Called when the player selection changes.
         *
         * @param count Number of players selected.
         */
        public void onPlayerSelectionChange(int count);
    }
}