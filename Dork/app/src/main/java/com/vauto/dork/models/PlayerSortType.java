package com.vauto.dork.models;

/**
 * Sort types allowed by player API calls.
 * Created by robert on 4/15/15.
 */
public enum PlayerSortType {
    Alphabetical,
    Rating,
    /**
     * Order for players as if they were being written on the board.
     */
    Board,
    /**
     * Reverse of {@link #Board}
     */
    Salty
}
